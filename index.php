<?php
session_start();
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Na Palma</title>
        <!-- CSS only -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
        <!-- JavaScript Bundle with Popper -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
    </head>
<style> 
            p {
                font-size: 12px; 
              
            }

            h5{
                font-weight: bold; /*ponto-e-vírgula é facultativo */
                color: #000000;
                background: #f0f0f0;
            }

            body {
                color: #000000;
                background: #FFFFFF;
                font-weight: bold; /*ponto-e-vírgula é facultativo */
            }
            .navblack{
                background-color:#000000 !important;
            }
            h1{
                font-weight: bold; /*ponto-e-vírgula é facultativo */
            }

            h3 {
                font-size: 20px; 
                color: #F5FFFA;
            }

            h2 {
                font-size: 20px; 
                color: #F5FFFA;
            }

            
</style>

<body>

<nav class="navbar navbar-expand-lg bg-light">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">Na Palma</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="col-4 d-flex justify-content-end align-items-center">
        <a class="link-secondary" href="#" aria-label="Search">
        </a>
        <a class="btn btn-sm btn-outline-secondary" href="login.php">Entrar</a>
      </div>
  </div>
</nav>



<div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="false">
  <div class="carousel-indicators">
    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
  </div>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="images/carossel_f.png" class="img-fluid d-block w 10" alt="...">
      


      <div class="carousel-caption d-none d-md-block">
        <h5>FACEBOOK</h5>
        <h3>Quer gerenciar suas contas do Facebook de forma fácil? Confira o Na Palma para facilitar a publicação cruzada entre contas. Alcance o público certo..</h3>
      </div>
    </div>
    <div class="carousel-item">
    <img src="images/carossel_inst.png" class="img-fluid d-block w 10" alt="...">
      <div class="carousel-caption d-none d-md-block">
        <h5>INSTAGRAM</h5>
        <h2>Quer gerenciar suas contas do Instagram forma fácil? Confira o Na Palma para facilitar a publicação cruzada entre contas. Alcance o público certo..</h2>
      </div>
    </div>
    <div class="carousel-item">
    <img src="images/carossel_g.png" class="img-fluid d-block w 10" alt="...">
      <div class="carousel-caption d-none d-md-block">
        <h5>MUITOS JÁ CONHECEM E OUTROS NUNCA OUVIRAM FALAR EM CONEXÃO CRUZADA!</h5>
        <p></p>
      </div>
    </div>
  </div>
  <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Previous</span>
  </button>
  <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Next</span>
  </button>
</div>

<section class="py-5 text-center container">
    <div class="row py-lg-5">
      <div class="col-lg-6 col-md-8 mx-auto">
      <svg xmlns="http://www.w3.org/2000/svg" width="100" height="50" fill="currentColor" class="bi bi-bookmark-star-fill" viewBox="0 0 16 16">
  <path fill-rule="evenodd" d="M2 15.5V2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v13.5a.5.5 0 0 1-.74.439L8 13.069l-5.26 2.87A.5.5 0 0 1 2 15.5zM8.16 4.1a.178.178 0 0 0-.32 0l-.634 1.285a.178.178 0 0 1-.134.098l-1.42.206a.178.178 0 0 0-.098.303L6.58 6.993c.042.041.061.1.051.158L6.39 8.565a.178.178 0 0 0 .258.187l1.27-.668a.178.178 0 0 1 .165 0l1.27.668a.178.178 0 0 0 .257-.187L9.368 7.15a.178.178 0 0 1 .05-.158l1.028-1.001a.178.178 0 0 0-.098-.303l-1.42-.206a.178.178 0 0 1-.134-.098L8.16 4.1z"/>
</svg>
        <p class="lead text">Na Palma, que vem para atender o desejo de todo profissional, proporcionando a praticidade em receber e analisar os dados de forma fácil, rápida e eficaz.</p>
      </div>
    </div>
  </section>

  <div class="row mb-2">
    <div class="col-md-6">
      <div class="row g-0 border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
        <div class="col p-4 d-flex flex-column position-static">
            <br>
          <h3 class="mb-0">Somente AQUI</h3>
          <br>
          <p class="card-text mb-auto">Você terá acesso a dados, relatórios e informações de fácil entendimento e o melhor de tudo é que com poucos cliques você consegue realizar tudo isso.
</p>
        </div>
        <div class="col-auto d-none d-lg-block">
        <img src="images/relatorioo.png" alt="" class="img-thumbnail">
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="row g-0 border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
        <div class="col p-4 d-flex flex-column position-static">
        <h3 class="mb-0">ATENDIMENTO</h3>
          <br>
          <p class="mb-auto">Centralizamos o relacionamento com nossos clientes, onde além de visualizar seus dashboards e resultados eles poderão acessar o SAC que habilitará um chatbot para contato com os consultores. </p>
        </div>
        <div class="col-auto d-none d-lg-block">
        <img src="images/chatbott.png" alt="..." class="img-thumbnail">

        </div>
      </div>
    </div>
  </div>


    <div class="card" style="width: 70rem;">
  <img src="images/inov.png" class="img-fluid card-img-top" alt="...">
  <div class="card-body">
    
    <a href="cadastrar.php" class="btn btn-primary">Quero começar!</a>
  </div>
</div>

<footer class="my-5 pt-5 text-muted text-center text-small">
    <p class="mb-1">© 2021-2021 Company NaPalma</p>
    <ul class="list-inline">
      <li class="list-inline-item"><a href="#">Privacidade</a></li>
      <li class="list-inline-item"><a href="#">Termos</a></li>
      <li class="list-inline-item"><a href="#">Suporte</a></li>
    </ul>
  </footer>

  
  

</body>

